# SensorApp

This application is meant to get different data from different sensors and to save them in a csv file on an Android smartphone.  
The data that are saved are from GPS, accelerometer, gyroscope, magnetometer, and constraint probes through an Arduino board.

This application has not been tested because of problems with Cordova. We were not able to run the app because Cordova couldn't find Java 8. It is highly possible that it doesn't work.

## Prerequisite

* Apache Cordova framework,
* All the cordova dependencies,
* Arduino device and constraint probes.

## Installation

* Install [Cordova](https://cordova.apache.org/#getstarted),
* Depending on the platform you wants to develop for, you will have to install some dependencies. See them for the [Android platform](https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html#installing-the-requirements),
* Assemble the Arduino circuit.

## How to run the app

When the [project configuration](https://cordova.apache.org/docs/en/latest/guide/platforms/android/index.html#project-configuration) is done, go in your terminal in the project folder and run the command :

```bash
cordova run android
```

If you want to run the app on an Android device, run :

```bash
cordova run android --device
```

When the app is running on the spartphone, you can unplug it from the computer and connect the Arduino board. See [Arduino for SensorApp documentation](https://gitlab.imt-atlantique.fr/commande-entreprise-20/arduino) for further information.

For all configuration problems, see [Apache Cordova help](https://cordova.apache.org/docs/en/latest/), which provides all informations.  
Cordova is a tough framework and it is quite difficult to make it run.